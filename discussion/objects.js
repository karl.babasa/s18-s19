//[SECTION] Objects

//Objects -> is a collection of related data and/or funtionalities. the purpose of an object is not only to store nultiple data-set but alse to represent real world object.

//SYNTAX: 
	/*let/const variableName = {
		key/property: value
	}*/

	//Information stored in objects are represented in key:value pairing.

	//let use this example to describe a real-world item/object
	let cellphone = {
		name: 'Nokie 3210', 
		manufactureDate: '1999',
		price: 5000
	}; // 'key' -> is also mostly referred to as a 'property' of an object.

	console.log(cellphone);
	console.log(typeof cellphone);

	//How to store multiple objects, 

	//you can use an array structure to store them.
	let users = [
		{
			name: 'Anna',
			age: '23'
		},
		{
			name: 'Nicole',
			age: '18'
		},
		{
			name: 'Smith',
			age: '42'
		},
		{
			name: 'Pam',
			age: '13'
		},
		{
			name: 'Anderson',
			age: '26'
		}	
	];

	console.log(users);
	//now there is an alternative way when displaying values of an object collection.
	console.table(users);

	//Complex examples of JS objects

	//NOTE: Different data types may be stored in an object's property creating more complex data structures

	//When using JS objects, you can also insert 'Methods'.

	// ..Methods -> are useful for creating reusable function that can perform task related to an object.
	let friend = {
		//properties
		firstName: 'Joe', //String
		lastName: 'Smith',
		isSingle: true, //boolean
		emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
		address: { //Objects
			city:'Austin',
			state: 'Texas',
			country: 'USA'
		},
		//methods
		introduce: function() {
			console.log('Hello, meet my new Friend');
		},
		advice: function() {
			console.log('Give friend an advice to move on.')
		},
		wingman: function() {
			console.log('Hype friend when meeting new people.')
		}
	}
	console.log(friend);

	//[SECTION] ALTERNATE APPROACHES on HOW TO DECLARE AN OBJECT IN JAVASCRIPT

		//1. How to create object an Object using a constructor function?

		//Constructor function -> we will need to create a reusable function that will declare a blueprint to describe the anatomy of an object that we wish to create.

		//to declare properties within a constructor function function, you have to be familiar wuth the 'this' keyword

		function Laptop(name, year, model) {
			this.name = name;
			this.manufactureOn = year;
			this.model = model;
		};

		//'this' -> NOT A VARIABLE but a *keyword*
		//this -> refers to the object in this example, 'this' refers to the global object *Laptop* because this keyword is within the scope of laptop contructor function. 

		//to create a new object that has the properties stated above, we will use the 'new' keyword for us to be able to create a new instance of an object.

		let laptop1 = new Laptop('Sony', 2008, 'viao');
		let laptop2 = new Laptop('Apple', 2019, 'Mac')
		let laptop3 = new Laptop('HP', 2015, 'hp');
		console.log(laptop1);
		console.log(laptop2);

		//what if i would want to store them in a single container and display them in a tavular format.

		let gadget = [laptop1, laptop2, laptop3];
		console.table(gadget);
		//==> use and benefits: this is usefull when creating 'instances' of several object that have same data structures.

		//instance -> this refers ti a concrete occurence of any object which emphasizes on the distinct/unique identity of the object/subject.

		//exaple#2:

		// lets create an instance of multiple pokemon characters.

		//use the 'this' keyword to associate each property with the global object and bind them with the respective values
		function Pokemon(name, type, level, trainer) {
			//properties
			this.pokemonName = name;
			this.pokemonType = type;
			this.pokemonHealth = 2 * level;
			this.pokemonLevel = level;
			this.owner = trainer;
			//methods
			this.tackle = function(target) {
				console.log(this.pokemonName + ' tackled ' + target.pokemonName);
			};
			this.greetings = function() {
				console.log(this.pokemonName + ' says Hello!');
			};
		}

		//create instance of a pokemon using the constructor function

		let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash Ketchum');
		let rattata = new Pokemon('Rattata', 'normal', 4, 'Misty');
		let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak');


		// display all pokemon in console in a table format

		let pokemons = [pikachu, rattata, snorlax];
		console.table(pokemons);

		//[SECTION] How to access Object Properties?

		//using '.' dot notation you can access properties of an object
		//SYNTAX: objectName.propertyName
		console.log(snorlax.owner);
		console.log(pikachu.pokemonType);
		//practice accessing a method of an object
		pikachu.tackle(rattata);
		snorlax.greetings();


		//[ALTERNATE APPROACH]
		//[] square bracket as an alternate approach
		//SYNTAX: objectName['property name']
		console.log(rattata['owner']);
		console.log(rattata['pokemonLevel']);


		//Which is the BEST USE CASE, dot notation or square bracket?

		//we will stick with dot notations when accessing properties of an object as our convention managing properties of an object.

		//we will use square bracket when dealing with indexes of an array.

	//[ADDITIONAL KNOWLEDGE]:

		//KEEP this in mind, you can write and declare objects this way:

		//to create an objects, you will use object literals '{}'
		
		let trainer = {}; //empty object
		console.log(trainer);

		//will i be able to add properties from outside an object? YES!

		trainer.name = 'Ash Ketchum';
		console.log(trainer);
		trainer.friends = ['Misty', 'Brock', 'Tracey'];
		console.log(trainer); //array

		//method

		trainer.speak = function() {
			console.log('Pikachu, I choose you!');
		}

		trainer.speak();

		/*console.log(trainer);*/
		//possible approach